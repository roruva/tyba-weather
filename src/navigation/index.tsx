import {
  createAppContainer,
  createStackNavigator,
  createSwitchNavigator
} from "react-navigation";

// Screens
import {
  HomeScreen,
  LogInScreen,
  SignInScreen,
  SplashScreen
} from "../screens";
import { Colors } from "../styles/Colors";

// Main Stack
const AppStackNavigation = createStackNavigator(
  {
    Home: HomeScreen
  },
  {
    initialRouteName: "Home",
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: Colors.green
      },
      headerTintColor: Colors.snow,
      headerBackTitle: null
    }
  }
);
// Auth Stack
const AuthStackNavigation = createStackNavigator(
  {
    LogIn: LogInScreen,
    SignIn: SignInScreen
  },
  {
    initialRouteName: "LogIn",
    defaultNavigationOptions: {
      header: null
    }
  }
);

const AppContainer = createAppContainer(
  createSwitchNavigator(
    {
      SplashScreen: SplashScreen,
      App: AppStackNavigation,
      Auth: AuthStackNavigation
    },
    {
      initialRouteName: "SplashScreen"
    }
  )
);

export default AppContainer;
