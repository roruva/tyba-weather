/**
 * Splash screen
 * @format
 */

import React, { Component } from "react";
import { AsyncStorage, StatusBar, Text, View } from "react-native";

import { styles } from "./styles";
import { Colors } from "../../styles";

interface Props {
  navigation: any;
}
export default class SplashScreen extends Component<Props> {
  constructor(props: Props) {
    super(props);
    this.bootstrapApp();
  }

  bootstrapApp = async () => {
    const userToken = await AsyncStorage.getItem("userToken");

    this.props.navigation.navigate(userToken ? "Home" : "Auth");
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={Colors.primary} />
        <Text style={styles.appName}>Weather</Text>
      </View>
    );
  }
}
