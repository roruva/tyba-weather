/**
 * Home screen
 * @format
 */

import React, { Component } from "react";
import { Text, View } from "react-native";

import { styles } from "./styles";

interface Props {}
export default class HomeScreen extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Home screen!</Text>
      </View>
    );
  }
}
