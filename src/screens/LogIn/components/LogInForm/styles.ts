import { StyleSheet } from "react-native";
import { Typography, Colors } from "../../../../styles";

export const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    width: "70%"
  },
  title: {
    ...Typography.display
  }
});
