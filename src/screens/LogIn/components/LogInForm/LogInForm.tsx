/**
 * LogIn screen
 * @format
 */

import React, { Component } from "react";
import { Text, View } from "react-native";
import { Formik } from "formik";

import { styles } from "./styles";
import { Button, Input } from "../../../../shared/";

interface LogInFormErrors {
  email?: string;
  password?: string;
}
interface Props {}
export default class LogInForm extends Component<Props> {
  render() {
    return (
      <Formik
        initialValues={{ email: "", password: "" }}
        validate={values => {
          let errors: LogInFormErrors = {};
          if (!values.email) {
            errors.email = "Required";
          } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
          ) {
            errors.email = "Invalid email address";
          }
          if (!values.password) {
            errors.password = "Required";
          }
          return errors;
        }}
        onSubmit={values => console.log(values)}
      >
        {props => (
          <View style={styles.container}>
            <Input
              label={"Email"}
              error={props.errors.email}
              touched={props.touched.email}
              onChangeText={props.handleChange("email")}
              onBlur={props.handleBlur("email")}
              value={props.values.email}
            />
            <Input
              label={"Password"}
              error={props.errors.password}
              touched={props.touched.password}
              onChangeText={props.handleChange("password")}
              onBlur={props.handleBlur("password")}
              value={props.values.password}
            />
            <Button text={"log In"} onPress={props.handleSubmit} />
          </View>
        )}
      </Formik>
    );
  }
}
