/**
 * LogIn screen
 * @format
 */

import React, { Component } from "react";
import { Text, View, TouchableWithoutFeedback, Keyboard } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import { styles } from "./styles";
import LogInForm from "./components/LogInForm/LogInForm";

interface Props {}
export default class LogInScreen extends Component<Props> {
  render() {
    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <KeyboardAwareScrollView contentContainerStyle={styles.container}>
          <View style={styles.container}>
            <Text style={styles.title}>Welcome to Tyba Weather!</Text>
            <Text style={styles.subheading}>Log In</Text>
            <LogInForm />
          </View>
        </KeyboardAwareScrollView>
      </TouchableWithoutFeedback>
    );
  }
}
