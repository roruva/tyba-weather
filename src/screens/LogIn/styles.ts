import { StyleSheet } from "react-native";
import { Typography } from "../../styles";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "100%"
  },
  title: {
    ...Typography.display,
    marginBottom: 40
  },
  subheading: {
    ...Typography.subheading,
    marginBottom: 30
  }
});
