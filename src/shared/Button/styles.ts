import { StyleSheet } from "react-native";
import { Colors, Typography } from "../../styles";

export const styles = StyleSheet.create({
  button: {
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 20,
    backgroundColor: Colors.primary,
    width: "100%",
    marginTop: 20
  },
  btnText: {
    ...Typography.body,
    fontWeight: "600",
    textAlign: "center",
    textTransform: "uppercase",
    color: Colors.snow
  }
});
