/**
 * Input component
 * @format
 */

import React, { Component, FormEvent } from "react";
import { Text, TouchableHighlight } from "react-native";

import { styles } from "./styles";

interface Props {
  color?: string;
  text: string;
  onPress: any;
}
export default class Button extends Component<Props> {
  render() {
    const { color = "primary", text, onPress } = this.props;
    return (
      <TouchableHighlight style={styles.button} onPress={onPress}>
        <Text style={styles.btnText}>{text}</Text>
      </TouchableHighlight>
    );
  }
}
