/**
 * Input component
 * @format
 */

import React, { Component } from "react";
import { Text, View, TextInput } from "react-native";

import { styles } from "./styles";

interface Props {
  label: string;
  onChangeText: (fieldKey: string) => void;
  onBlur: (evt: Event) => void;
  value: any;
  error?: any;
  touched?: any;
}
export default class Input extends Component<Props> {
  render() {
    const { label, onChangeText, onBlur, value, error, touched } = this.props;
    const iptStyles = [styles.input, error ? styles.inputError : null];

    return (
      <View style={styles.container}>
        {label && <Text style={styles.label}>{label}</Text>}
        <TextInput
          style={styles.input}
          onChangeText={onChangeText}
          onBlur={onBlur}
          value={value}
        />
        {error && touched && <Text style={styles.error}>{error}</Text>}
      </View>
    );
  }
}
