import { StyleSheet } from "react-native";
import { Colors, Typography } from "../../styles";

export const styles = StyleSheet.create({
  container: {
    alignItems: "flex-start",
    width: "100%",
    marginTop: 10,
    marginBottom: 10
  },
  label: {
    ...Typography.body,
    fontWeight: "600",
    marginBottom: 5,
    marginLeft: 5
  },
  input: {
    width: "100%",
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: Colors.grey,
    borderRadius: 20,
    paddingHorizontal: 10,
    paddingVertical: 8,
    ...Typography.body
  },
  inputError: {
    borderColor: Colors.red
  },
  error: {
    color: Colors.red,
    ...Typography.small,
    marginTop: 3,
    marginLeft: 5
  }
});
