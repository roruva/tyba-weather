/**
 * App - Main file
 *
 * @format
 */

import React, { Component } from "react";

import AppContainer from "./navigation";

interface Props {}
export default class App extends Component<Props> {
  render() {
    return <AppContainer />;
  }
}
