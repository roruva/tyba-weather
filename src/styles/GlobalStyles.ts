export const Typography = {
  display: {
    fontSize: 22
  },
  title: {
    fontSize: 22
  },
  subheading: {
    fontSize: 18,
    fontWeight: "600"
  },
  body: {
    fontSize: 14
  },
  smallBold: {
    fontSize: 11,
    fontWeight: "600"
  },
  small: {
    fontSize: 11
  }
};
