import { Colors } from "./Colors";
import { Typography } from "./GlobalStyles";
import { Theme } from "./Theme";

export { Colors, Theme, Typography };
