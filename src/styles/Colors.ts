export const Colors = {
  // App colors
  primary: "#098FD7",
  accent: "#EC4E20",
  gold: "#FF9505",
  green: "#08AF2D",
  red: "#D0021D",
  blue: "#007AFF",
  yellow: "#E8CA20",
  // Scale gray
  snow: "#FFF",
  black: "#000",
  darkSilver: "#7A7A7A",
  silver: "#C3C3C3",
  grey: "#E8E8E8",
  grey2: "#DADADA",
  smoke: "#FAFAFA",
  transparent: "transparent"
};
